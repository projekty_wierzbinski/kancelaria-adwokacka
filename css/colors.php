<?php
	function hex2rgb($color , $opacity = 0.7 ) {
		$color = str_replace("#", "", $color);
		$color = 'rgba('.implode( "," , array_map( 'hexdec', str_split( $color,2 ) ) ).','.$opacity.')';
		return $color;
	}

	$color = isset( $_REQUEST['color'] ) ? $_REQUEST['color'] : 'blue';

	switch ( $color ) {

		case 'pink':
			$main = '#e84f82';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#260510';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#fcedf2';
			$dark = hex2rgb( '#7a1637' , '0.85' );						
		break;

		case 'red':
			$main = '#e8594f';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#260705';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#fceeed';
			$dark = hex2rgb( '#7a1d16' , '0.85' );						
		break;

		case 'purple':
			$main = '#d14fe8';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#210526';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#faedfc';
			$dark = hex2rgb( '#6b167a' , '0.85' );						
		break;

		case 'deep-purple':
			$main = '#874fe8';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#110526';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#f3edfc';
			$dark = hex2rgb( '#3b167a' , '0.85' );			
		break;

		case 'indigo':
			$main = '#4f66e8';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#050a26';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#edf0fc';
			$dark = hex2rgb( '#16257a' , '0.85' );
			
		break;

		case 'cyan':
			$main = '#4fd6e8';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#052226';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#edfbfc';
			$dark = hex2rgb( '#166f7a' , '0.85' );
		break;

		case 'teal':
			$main = '#4fe8d9';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#052623';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#edfcfb';
			$dark = hex2rgb( '#167a70' , '0.85' );
		break;

		case 'green':
			$main = '#4caf50';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#052606';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#edfcee';
			$dark = hex2rgb( '#167a19' , '0.85' );
		break;

		case 'amber':
			$main = '#e8c24f';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#261e05';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#fcf9ed';
			$dark = hex2rgb( '#7a6116' , '0.85' );
		break;

		case 'orange':
			$main = '#ff9800';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#261905';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#fcf6ed';
			$dark = hex2rgb( '#7a5216' , '0.85' );
		break;

		case 'deep-orange':
			$main = '#ff5722';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#260d05';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#fcf1ed';
			$dark = hex2rgb( '#7a2d16' , '0.85' );
		break;

		case 'brown':
			$main = '#795548';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#260e05';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#fcf1ed';
			$dark = hex2rgb( '#6a2c15' , '0.85' );
		break;

		case 'light-green':
			$main = '#8bc34a';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#172605';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#f5fced';
			$dark = hex2rgb( '#4c7a16' , '0.85' );
		break;

		case 'lime':
			$main = '#c0ca33';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#242605';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#fbfced';
			$dark = hex2rgb( '#747a16' , '0.85' );
		break;

		case 'blue':
		default:
			$main = '#50c1e9';
			$mainrgb1 = hex2rgb( $main, '0.7' );
			$mainrgb2 = hex2rgb( $main, '0.6' );
			$secondary = '#051f27';
			$secondaryrgb = hex2rgb( $secondary , '0.8' );
			$light = '#edf9fd';
			$dark = hex2rgb( '#16607a' , '0.85' );
		break;
	}

header("Content-type: text/css");?>
@charset "utf-8";
::selection {
	background:<?php echo $main;?>; 
}

::-moz-selection {
	background:<?php echo $main;?>; 
}

a, .mendi-portfolio-item:hover figure figcaption h6 a:hover, .mendi-portfolio-item:hover figure figcaption p a:hover, .mendi-facts .mendi-facts-icon-holder i, .mendi-list li:before, .mendi-contact address:before, .mendi-contact abbr:before, .mendi-contact abbr a:hover, .mendi-header-with-topbar .header-top ul.header-contact li a:hover, .mendi-blog-item .entry-meta .posted-on time span, .mendi-blog-item .entry-footer p a:hover, .mendi-blog-item .entry-header h4 a:hover, .widget.widget_recent_entries ul li a:hover, .widget ul li a:hover, .widget.widget_categories ul li:hover:before, .paging-navigation .pagination .page-numbers.current, .mendi-portfolio-meta-details li.links a:hover, .mendi-icon-with-title.style3 .mendi-icon-holder i, .mendi-team h6, .mendi-team .mendi-social-links li a:hover i, .mendi-icon-with-title.style5 .mendi-icon-holder i, .mendi-error404 .mendi-error-info i {	color:<?php echo $main;?>;}

.header-top ul li i, .header-top ul li a:hover {
	color:<?php echo $main;?>; 
}

.main-nav > ul > li > a:hover, .header-main.sticky .main-nav > ul > li > a:hover, .main-nav > ul > li.current_page_item > a, .header-main.sticky .main-nav > ul > li.current_page_item > a, .main-nav ul.mendi-sub-menu li.current_page_item > a, .mendi-mega-menu-container ul li.current_page_item a {
	color:<?php echo $main;?>; 
}

.mendi-heading h1:before, .mendi-heading h2:before, .mendi-heading h3:before, .mendi-heading h4:before, .mendi-heading h5:before, .mendi-portfolio-details-slideshow-nav a:hover {
	background:<?php echo $main;?>; 
}

button, input[type="button"], input[type="reset"], input[type="submit"], .mendi-icon-with-title.style1 .mendi-icon-holder, .mendi-icon-with-title h6:before, .mendi-button, .mendi-facts:hover .mendi-facts-icon-holder, .mendi-icon-with-title.style2:hover .mendi-icon-holder, .mendi-blog-thumb-style.skin, .widget h2.widget-title:before, .widget.widget_tag_cloud .tagcloud a:hover, .mendi-sidebar-holder .widget .mendi-newsletter input[type="submit"]:hover, .paging-navigation .pagination .page-numbers:hover, blockquote.style1:before, ol.comment-list li.comment .reply a:hover, .mendi-contact-with-large-icon .mendi-contact-icon-holder, .mendi-portfolio-filter ul li a:hover, .mendi-portfolio-filter ul li a.current, .mendi-blog-item .post-thumbnail .post-format, .mendi-tabs-boxed .mendi-tabs-nav li a:hover, .mendi-tabs-boxed .mendi-tabs-nav li a.current, .mendi-icon-with-title.style4:hover .mendi-icon-holder, .mendi-team .mendi-social-links:before, .mendi-facts.style2 h6:before, .mendi-blog-thumb-style2, ol.mendi-planner-list > li h6:before, ol.mendi-planner-list > li:hover:before, .mendi-progress-bar-wrapper .mendi-progress-bar, .mendi-accordion-holder .mendi-accordion-panel.current .mendi-accordion-title .mendi-accordion-arrow, .mendi-accordion-holder .mendi-accordion-panel:hover .mendi-accordion-title .mendi-accordion-arrow, .hvr-shutter-out-horizontal:before, .mendi-icon-with-title.style7 .mendi-icon-holder, .mendi-button.stroke-style:hover, #mendi-vertical-nav .mendi-vertical-nav-dot, #mendi-vertical-nav .mendi-vertical-nav-label, .mendi-top:hover, .mendi-owl-carousel-custom-nav a:hover, #infscr-loading, #portfolio-ajax-loader {
	background-color:<?php echo $main;?>; 
}

.mendi-icon-with-title.style2 .mendi-icon-holder, .widget.widget_tag_cloud .tagcloud a:hover, .mendi-sidebar-holder .widget .mendi-newsletter input[type="submit"], .paging-navigation .pagination .page-numbers:hover, ol.comment-list li.comment .reply a:hover, .mendi-portfolio-filter ul li a:hover, .mendi-portfolio-filter ul li a.current, .mendi-tabs-boxed .mendi-tabs-nav li, ol.mendi-planner-list > li:hover:before, .mendi-accordion-holder .mendi-accordion-panel.current .mendi-accordion-title .mendi-accordion-arrow, .mendi-accordion-holder .mendi-accordion-panel:hover .mendi-accordion-title .mendi-accordion-arrow, .mendi-button.stroke-style:hover, #mendi-vertical-nav a.is-selected .mendi-vertical-nav-dot, .mendi-owl-carousel-custom-nav a:hover, #mendi-masthead.mendi-left-side-header, .mendi-portfolio-details-slideshow-nav a:hover {
	border-color:<?php echo $main;?>; 
}

.widget.mendi-flickr-widget ul li a:before {
	background-color:<?php echo $mainrgb1;?>; 
}

.mendi-blog-thumb-style .post-thumbnail:before {
	background:<?php echo $mainrgb2;?>; 
}

.mendi-blog-thumb-style.dark, footer#mendi-footer .mendi-footer-widgets, button:hover, input[type="button"]:hover, input[type="reset"]:hover, input[type="submit"]:hover, button:focus, input[type="button"]:focus, input[type="reset"]:focus, input[type="submit"]:focus, .mendi-header-with-topbar .header-top, .mendi-blog-item:hover .post-thumbnail .post-format, .mendi-button.stroke-style.light:hover {
	background-color:<?php echo $secondary;?>; 
}

.mendi-button.stroke-style.light:hover {
	border-color:<?php echo $secondary;?>; 
}

.mendi-icon-with-title.style1:hover .mendi-icon-holder, .mendi-icon-with-title.style3:hover .mendi-icon-holder {
	background:<?php echo $secondary;?>; 
}

.mendi-portfolio-item figure figcaption, .mendi-row-overlay:before {
	background:<?php echo $secondaryrgb;?>; 
}

blockquote {
	background-color:<?php echo $light;?>; 
}

.mendi-testimonial blockquote:before {
	border-top-color:<?php echo $light;?>; 
}

.mendi-title-overlay:before {
	background:<?php echo $dark;?>; 
}